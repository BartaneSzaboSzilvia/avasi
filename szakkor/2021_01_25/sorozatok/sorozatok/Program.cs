﻿using System;
using System.IO;

namespace sorozatok
{
    class Program
    {
        struct Sablon
        {
            public string datum;
            public string cim;
            public int evad;
            public int epizod;
            public int hossz;
            public bool nezte;
        }

        static void Main(string[] args)
        {
            Sablon[] sablon = new Sablon[400];
            
            StreamReader be = new StreamReader("lista.txt");
            int n = 0;

            while (!be.EndOfStream)
            {
                sablon[n].datum = be.ReadLine();
                sablon[n].cim = be.ReadLine();

                string sor = be.ReadLine();
                string[] adat = sor.Split('x');
                
                sablon[n].evad = Convert.ToInt32(adat[0]);
                sablon[n].epizod = Convert.ToInt32(adat[1]);
                sablon[n].hossz = Convert.ToInt32(be.ReadLine());
                int megtekintette = Convert.ToInt32(be.ReadLine());
                if (megtekintette == 0)
                {
                    sablon[n].nezte = false;
                }
                if (megtekintette == 1)
                {
                    sablon[n].nezte = true;
                }
     
                n++;
            }
            be.Close();

            //1. feladat vége

            Console.WriteLine("2. feladat");
            
            int db = 0;
            
            for (int i = 0; i < n; i++)
            {
                if (sablon[i].datum != "NI")
                {
                    db++;
                }
            }

            Console.WriteLine($"A listában {db} db vetítési dátummal rendelkező epizód van.");
            
            //2. feladat vége

            Console.WriteLine("\n3. feladat");

            int latta = 0;
            for (int i = 0; i < n; i++)
            {
                if (sablon[i].nezte ==false)
                {
                    latta++;
                }
            }
            
            double szazalek = (Convert.ToDouble(latta) / Convert.ToDouble(n)) * 100;
            
            Console.WriteLine($"A listában lévő epizódok {szazalek:0.00} % -át látta.");
            
            //3. feladat vége

            //teszt komment


        }
    }
}
